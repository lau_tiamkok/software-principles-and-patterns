# Software design pattern

In software engineering, a design pattern is a general reusable solution to a commonly occurring problem within a given context in software design. A design pattern is not a finished design that can be transformed directly into source or machine code. It is a description or template for how to solve a problem that can be used in many different situations. Patterns are formalized best practices that the programmer can use to solve common problems when designing an application or system. Object-oriented design patterns typically show relationships and interactions between classes or objects, without specifying the final application classes or objects that are involved. Patterns that imply object-orientation or more generally mutable state, are not as applicable in functional programming languages.

Design patterns reside in the domain of modules and interconnections. At a higher level there are architectural patterns that are larger in scope, usually describing an overall pattern followed by an entire system

References:

1. http://en.wikipedia.org/wiki/Software_design_pattern

2. http://www.fluffycat.com/PHP-Design-Patterns/


# Understanding Design Patterns in JavaScript

## A Note About Classes in JavaScript

When reading about design patterns, you'll often see references to classes and objects. This can be confusing, as JavaScript does not really have the construct of "class"; a more correct term is "data type".

* Data Types in JavaScript

JavaScript is an object-oriented language where objects inherit from other objects in a concept known as prototypical inheritance. A data type can be created by defining what is called a constructor function, like this:

```
function Person(config) {
    this.name = config.name;
    this.age = config.age;
}
 
Person.prototype.getAge = function() {
    return this.age;
};
 
var tilo = new Person({name:"Tilo", age:23 });
console.log(tilo.getAge());
```

* Dealing with Privacy

Another common issue in JavaScript is that there is no true sense of private variables. However, we can use closures to somewhat simulate privacy. Consider the following snippet:

```
var retinaMacbook = (function() {
 
    //Private variables
    var RAM, addRAM;
 
    RAM = 4;
 
    //Private method
    addRAM = function (additionalRAM) {
        RAM += additionalRAM;
    };
 
    return {
 
        //Public variables and methods
        USB: undefined,
        insertUSB: function (device) {
            this.USB = device;
        },
 
        removeUSB: function () {
            var device = this.USB;
            this.USB = undefined;
            return device;
        }
    };
})();
```

In the example above, we created a retinaMacbook object, with public and private variables and methods. This is how we would use it:

``
retinaMacbook.insertUSB("myUSB");
console.log(retinaMacbook.USB); //logs out "myUSB"
console.log(retinaMacbook.RAM) //logs out undefined
```

References:

1. http://code.tutsplus.com/tutorials/understanding-design-patterns-in-javascript--net-25930
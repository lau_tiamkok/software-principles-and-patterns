# The builder pattern

In the Builder Pattern a director and a builder work together to build an object. The director controls the building and specifies what parts and variations will go into an object. The builder knows how to assemble the object given specification.

In short, builder pattern **makes and returns one object various ways**.

References:

1. http://www.fluffycat.com/PHP-Design-Patterns/Builder/

2. http://en.wikipedia.org/wiki/Builder_pattern

3. http://dsheiko.com/weblog/design-patterns-by-php-and-js-es5-examples#builder

Videos: 

1. https://www.youtube.com/watch?v=9XnsOpjclUg&index=8&list=PLF206E906175C7E07 (java)
<!DOCTYPE html>
<html>

<script>

    var Builder = function() {
    var a = "defaultA";
    var b = "defaultB";

        return {
            withA : function(anotherA) {
              a = anotherA;
              return this;
            },
            withB : function(anotherB) {
              b = anotherB; 
              return this;
            },
            build : function() {
              return "A is: " + a +", B is: " + b;
            }
        };
    };

    var builder = new Builder();

    console.log(builder.build());

    var first = builder.withA("a different value for A").withB("a different value for B").build();

    var second = builder.withB("second different value for B").build();

    var third = builder.withA("now A is different again").build();

    console.log(first);
    console.log(second);
    console.log(third);

</script>

</head>
<body>
</body>
</html>
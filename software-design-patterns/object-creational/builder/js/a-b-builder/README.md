# Builder Design Pattern in JavaScript

It is useful for starting off with a set of defaults, but is clear when we want to override particular values. 

References:

1. https://www.thekua.com/atwork/2013/04/a-builder-pattern-implementation-in-javascript/
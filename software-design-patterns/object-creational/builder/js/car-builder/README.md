# Builder Design Pattern in JavaScript

## Definition
Separate the construction of a complex object from its representation so that the same construction process can create different representations.


## Participants

The objects participating in this pattern are: 

* Director -- In sample code: Shop
    constructs products by using the Builder's multistep interface

* Builder -- not used in JavaScript
    declares a multistep interface for creating a complex product

* ConcreteBuilder -- In sample code: CarBuilder, TruckBuilder
    implements the multistep Builder interface
    maintains the product through the assembly process
    offers the ability to retrieve the newly created product

* Products -- In sample code: Car, Truck
    represents the complex objects being assembled

References:

1. http://www.dofactory.com/javascript/builder-design-pattern
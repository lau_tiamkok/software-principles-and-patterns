<?php
// Builder.
interface Builder
{
    function assemble();
}

// Model.
interface Model
{
    
}

// AKA Director
interface Engineer
{
    function __construct(Builder $Builder, Model $Model);
    function build();
}

class ConcreteBuilder implements Builder
{
    private $buffer;

    public function setAuthor($author)
    {
        $this->buffer .= $author . '<br/>';
    }
    public function setTitle($title)
    {
        $this->buffer .= $title . '<br/>';
    }
    public function setText($text)
    {
        if(is_array($text))
        {
            foreach ($text as $item) 
            {
                $this->buffer .= $item . '<br/>';
            }
        }
        else 
        {
            $this->buffer .= $text . '<br/>';
        }
    }

    public function assemble()
    {
        return $this->buffer;
    }
}

class ConcreteModel implements Model
{
    public $author = "George R. R. Martin";
    public $title = "A Song of Ice and Fire";
    public $text = ["Chapter 1", "Chapter 2"];
}

class ConcreteEngineer implements Engineer
{
    private $Builder;
    private $Model;
    
    public function __construct(Builder $Builder, Model $Model) 
    {
         $this->Builder = $Builder;
         $this->Model = $Model;
    }

    public function build()
    {
        $this->Builder->setAuthor($this->Model->author);
        $this->Builder->setTitle($this->Model->title);
        $this->Builder->setText($this->Model->text);
        return $this->Builder->assemble();
    }
}

$ConcreteBuilder = new ConcreteBuilder();
$ConcreteModel = new ConcreteModel();
$ConcreteEngineer = new ConcreteEngineer($ConcreteBuilder, $ConcreteModel);
echo $ConcreteEngineer->build();
<?php
// Component
interface Component
{
    public function getSetting( $defaults = [] );
}

// Composite
class ConcreteComposite implements Component
{
    private $Leaves = [];
    
    public function addLeaf( Component $Component )
    {
        array_push( $this->Leaves, $Component );
    }
    
    public function getRow( )
    {
        $defaults = [
            "id" 	=> 	null,
            "type"      =>      null
        ];
        
        // other codes.
        
        // return the setting as the dummy.
        return $this->getSetting( $defaults );
    }
    
    public function getSetting( $defaults = [] )
    {
        foreach ( $this->Leaves as $Leaf ) 
        {
            return $Leaf->getSetting( $defaults );
        }
    }
}

// Leaf
class SettingComponent implements Component
{
    public $options;
    public $type;
    
    public function  __construct( $options, $type = null )
    {
        $this->options = $options;
        $this->type = $type;
    }
    
    public function getSetting( $defaults = [] )
    {
        if( $this->type === 'array' )
        {
            return $this->arrayMergeValues( $defaults, $this->options );
        }
        else
        {
            return $this->arrayToObject( $this->arrayMergeValues( $defaults, $this->options ) );
        }
    }
    
    public function arrayMergeValues( $defaults = [], $options = [] )
    {
        // Set empty arrays for error & items.
        $items = [];

        // If the $defaults is empty then just return everything in $options
        if( count( $defaults ) == 0 ) return $options;

        // Loop the array.
        foreach( $defaults as $key => $value )
        {
            // If the default key's value is not an array and this key is present in config's key.
            if( isset( $options[$key] ) && !is_array( $value ) )
            {
                $items[$key] = $options[$key];
            }
            // If the default key's value is an array and it has at least one item in it, re-loop this method.
            elseif( isset( $options[$key] ) && is_array( $value ) && count( $value ) > 0 )
            { 
                $items[$key] = self::arrayMergeValues( $value,$options[$key] );
            }
            // If the default key's value is an array but nothing in it, take everything from config.
            elseif( isset( $options[$key] ) && is_array( $value ) && count( $value ) == 0 )
            {
                $items[$key] = $options[$key];
            }
             // Else use the default key's value.
            else 
            {
                $items[$key] = $value;
            }
        }

        // Merge the processed array with error array.
        // Return the result.
        return $items;
    }
    
    public function arrayToObject( $array = [] )
    {
        // If $array is not an array, let's make it array with one value of former $array.
        if ( !is_array( $array ) ) return $array;
        
        $object = new stdClass( );

        foreach( $array as $key => $value ) 
        {
            $key = ( string ) $key ;

            // Take 0 as a string of '0'.
            // Take other empty data as null.
            // Loop the method if it is an array.
            if( !is_array( $value ) && $value == '0' ) $object->$key = $value;
                else if( !is_array( $value ) && empty( $value ) ) $object->$key = null;
                    else $object->$key = is_array( $value ) ? self::arrayToObject( $value ) : $value;
        }

        return $object;
    }
}


// Client.
$SettingComponent = new SettingComponent( [
    "id" => 10
], 'array' );

$ConcreteComposite = new ConcreteComposite( );
$ConcreteComposite->addLeaf( $SettingComponent );

var_dump( $ConcreteComposite->getRow( ) );
<?php
// Component
interface Component
{
    public function toDo( );
}

// Composite
class ConcreteComposite implements Component
{
    private $Leaves = [];
    
    public function addLeaf( Component $Component )
    {
        array_push( $this->Leaves, $Component );
    }
    
    public function toDo( ) 
    {
        foreach ( $this->Leaves as $Leaf ) 
        {
            $Leaf->toDo( );
        }
    }
}

// Leaf 1
class ConcreteBlackLeaf implements Component
{
    public $string;
    
    public function  __construct( $input )
    {
        $this->string = $input;
    }
    
    private function replace( $input ) 
    {
        return str_replace( "*", "@", $input );
    }
    
    private function toUppercase( $input ) 
    {
        return strtoupper( $input );
    }
    
    public function toDo( ) 
    {
        echo $this->toUppercase( $this->replace( $this->string ) ) . '<br/>';
    }
}

// Leaf 2
class ConcreteBlueLeaf implements Component
{
    public $string;
    
    public function  __construct( $input )
    {
        $this->string = $input;
    }
    
    private function toLowercase( $input ) 
    {
        return strtolower( $input );
    }
    
    public function toDo( ) 
    {
        echo $this->toLowercase( $this->string ) . '<br/>';
    }
}


// Client.
$ConcreteLeaf1 = new ConcreteBlackLeaf( 'Hello * World!' );
$ConcreteLeaf2 = new ConcreteBlackLeaf( 'Hello * Universe!' );
$ConcreteLeaf3 = new ConcreteBlueLeaf( 'Hello Earth!' );

$ConcreteComposite = new ConcreteComposite( );
$ConcreteComposite->addLeaf( $ConcreteLeaf1 );
$ConcreteComposite->addLeaf( $ConcreteLeaf2 );
$ConcreteComposite->addLeaf( $ConcreteLeaf3 );
$ConcreteComposite->toDo( );
# The composite pattern

The composite pattern describes that a group of objects is to be treated in the same way as a single instance of an object. The intent of a composite is to "compose" objects into tree structures to represent part-whole hierarchies. Implementing the composite pattern lets clients treat individual objects and compositions uniformly.

This hierarchy is usually constituted by part-whole relationships (often composition in the strict sense or aggregation), and it can be viewed as a tree.

The power of the Composite pattern resides in a Client which refers only to a Component interface, the common abstraction between all kinds of tree elements, and it is  oblivious to changes in the underlying structure. The Client is not even aware of the hierarchical structure existence and it is passed a reference to the tree head.

The Composite responsibility is to build on its Component children's operations, and propagate their calls towards the bottom of the hierarchical graph until they reach Leafs.

**A Party of Four**

The Composite pattern is pretty simple when you consider the main four participants:

1. Component (IComponent):

    * Declares interface
    * Implements default behavior as appropriate
    * Declare an interface for access and managing its child components
    * Optionally defines an interface for accessing a components parent in the recursive structure and implements it if appropriate.

2. Leaf

    * Represents leaf objects in composition.
    * Defines behavior for primitive objects in the composition.

3. Composite

    * Defines behavior for components having children
    * Stores child components
    * Implements child-related operations in the Component interface

4. Client

    * Manipulates objects in the composition through the Component interface.

**Purpose of composite pattern: definition 1:**

The point of the composite pattern is to be able to treat a collection of objects as if it were a single object (e.g. for displaying it or writing it to a file). As you write yourself "print() method may follow Iterator pattern later" - well, the point of the composite pattern would be that you could call print() without having to worry about whether you're printing a single Component or have to iterate through an entire Tree.

**Purpose of composite pattern: definition 2:**

The point of composition is to delegate some responsibility to another strategy object that itself is abstract and has subclasses that contain the real specific logic. The superclass is simplified as it only deals with the strategy object, it will receive the strategy object as a parameter and invoke its method(s). New strategies can then be added just by extending the strategy object and that requires to changes to the superclass.

Without the composition you would be creating a deep three of superclasses and subclasses whenever you need to add new specific logic.

It�s about making the code more flexible without making changes to your superclass

**Benefits:**

* to make an architecture where single object instances and collections of these objects are treated uniformly.

References:

1. http://dsheiko.com/weblog/design-patterns-by-php-and-js-es5-examples#composite

2. http://www.craigsefton.com/programming/php-patterns-the-composite-pattern/

3. http://stackoverflow.com/questions/1254356/what-advantage-will-composite-pattern-bring-me-over-just-array

4. http://en.wikipedia.org/wiki/Composite_pattern

5. http://www.php5dp.com/the-composite-design-pattern-in-php-part-i-from-conceptual-to-practical/

6. http://www.giorgiosironi.com/2010/01/practical-php-patterns-composite.html

7. http://stackoverflow.com/questions/1254356/what-advantage-will-composite-pattern-bring-me-over-just-array
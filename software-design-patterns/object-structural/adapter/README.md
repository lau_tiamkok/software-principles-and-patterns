# Adapter patterns

**Participants**

The objects participating in this pattern are:

* **Client** -- 
    * In sample code: the run() function.
    * calls into Adapter to request a service

* **Adapter** -- 
    * In sample code: ShippingAdapter
    * implements the interface that the client expects or knows

* **Adaptee** -- 
    * In sample code: AdvancedShipping
    * the object being adapted
    * has a different interface from what the client expects or knows

References:

1. http://www.dofactory.com/javascript/adapter-design-pattern
2. http://dsheiko.com/weblog/design-patterns-by-php-and-js-es5-examples#adapter
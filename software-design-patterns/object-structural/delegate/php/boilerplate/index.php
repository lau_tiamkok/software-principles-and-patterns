<?php
interface Delegate
{
}

interface Delegator
{
    function addDelegate( Delegate $Delegate );
}

class ConcreteDelegate implements Delegate
{
    public function addItem( $name )
    {
        echo $name . '<br/>';
    }
}

class ConcreteDelegator implements Delegator
{
    private $Delegate;

    public function addDelegate( Delegate $Delegate )
    {
        $this->Delegate = $Delegate;
    }
    
    public function addItem( $name )
    {
        $this->Delegate->addItem( $name );
    }
}

$ConcreteDelegate = new ConcreteDelegate();
$ConcreteDelegator = new ConcreteDelegator();
$ConcreteDelegator->addDelegate( $ConcreteDelegate );

$ConcreteDelegator->addItem( "Jack" );
$ConcreteDelegator->addItem( "Jane" );
# The delegation pattern

The delegation pattern is a design pattern in object-oriented programming where an object, instead of performing one of its stated tasks, delegates that task to an associated helper object. There is an Inversion of Responsibility in which a helper object, known as a delegate, is given the responsibility to execute a task for the delegator. The delegation pattern is one of the fundamental abstraction patterns that underlie other software patterns such as composition (also referred to as aggregation), mixins and aspects.

Delegation can be considered as an extreme form of object composition that can always be used to replace inheritance.

The pattern allows:

* an application design where one object (Delegator) instead of performing a task, delegate it to an associated helper object (Delegates).

References:

1. http://dsheiko.com/weblog/design-patterns-by-php-and-js-es5-examples#delegate

2. http://en.wikipedia.org/wiki/Delegation_pattern
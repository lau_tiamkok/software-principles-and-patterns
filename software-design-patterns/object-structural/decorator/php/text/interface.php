<?php
// Why use an interface?

// Because then you can add as many decorators as you like and be assured that
// each decorator (or object to be decorated) will have all the required
// functionality.

// Interface
interface IDecoratedText
{
    public function __toString();
}

// Main class
class Text implements IDecoratedText
{
    protected $string;

    /**
     * @param string $string
     */
    public function __construct($string)
    {
        $this->string = $string;
    }

    public function __toString()
    {
        return $this->string;
    }
}

// Decorator
class LeetText implements IDecoratedText
{
    protected $text;

    public function __construct(IDecoratedText $text)
    {
        $this->text = $text;
    }

    public function __toString()
    {
        return strtr($this->text->__toString(), 'eilto', '31170');
    }
}

$text = new LeetText(new Text('Hello world'));
echo $text; // H3110 w0r1d

<?php
class Text
{
    protected $string;

    /**
     * @param string $string
     */
    public function __construct($string)
    {
        $this->string = $string;
    }

    public function __toString()
    {
        return $this->string;
    }
}

class LeetText
{
    protected $text;

    /**
     * @param Text $text A Text object.
     */
    public function __construct($text)
    {
        $this->text = $text;
    }

    public function __toString()
    {
        return strtr($this->text->__toString(), 'eilto', '31170');
    }
}

$text = new LeetText(new Text('Hello world'));
echo $text; // H3110 w0r1d

<?php
// Interface
interface Car
{
    function cost();
    function description();
}

// Main Class
class Suv implements Car
{
    function cost()
    {
        return 30000;
    }

    function description ()
    {
        return "Suv";
    }
}

// Main Decorator
abstract class CarFeature implements Car
{
    protected $car;

    function __construct(Car $car)
    {
        $this->car = $car;
    }

    abstract function cost();

    abstract function description();
}

// Sub Decorator
class SunRoof extends CarFeature
{
    function cost ()
    {
        return $this->car->cost() + 1500;
    }

    function description()
    {
        return $this->car->description() . ",  sunroof";
    }
}

class HighEndWheels extends CarFeature
{
    function cost ()
    {
        return $this->car->cost() + 2000;
    }

    function description()
    {
        return $this->car->description() . ",  high end wheels";
    }
}

// Create an object from one of the basic classes.
$basicCar = new Suv();

// Pass the object from the basic class as a parameter to the first feature class.
$carWithSunRoof = new SunRoof($basicCar);

// Run the methods on the last object that was created.
echo $carWithSunRoof->description();
echo " costs ";
echo $carWithSunRoof->cost();

// Result:
// Suv, sunroof costs 31500

// 1. Create an object from one of the basic classes.
$basicCar = new Suv();

// 2. Pass the object from the basic class as a parameter to the first feature class.
$carWithSunRoof = new SunRoof($basicCar);

// 3. Pass the object from the first feature class as a parameter to the second feature class.
$carWithSunRoofAndHighEndWheels = new HighEndWheels($carWithSunRoof);

// 4. Run the methods on the last object that was created.
echo $carWithSunRoofAndHighEndWheels->description();
echo " costs ";
echo $carWithSunRoofAndHighEndWheels->cost();

// And the result:
// Suv, sunroof, high end wheels costs 33500

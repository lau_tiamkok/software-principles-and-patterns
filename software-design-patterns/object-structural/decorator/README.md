# Decorator patterns

 The decorator pattern (also known as Wrapper, an alternative naming shared with the Adapter pattern) is a design pattern that allows behavior to be added to an individual object, either statically or dynamically, without affecting the behavior of other objects from the same class.[1] The decorator pattern is often useful for adhering to the Single Responsibility Principle, as it allows functionality to be divided between classes with unique areas of concern.

 Ref: https://en.wikipedia.org/wiki/Decorator_pattern

 We can use the decorator pattern when we just want to give some added responsibility to our base class. This design pattern is a great alternative to a sub‑classing feature for extending functionality with some added advantages.

 Ref: http://code.tutsplus.com/tutorials/design-patterns-the-decorator-pattern--cms-22641

 In the Decorator pattern, a class will add functionality to another class, without changing the other classes' structure.

 Ref: https://sourcemaking.com/design_patterns/decorator/php

 I would suggest that you also create a unified interface (or even an abstract base class) for the decorators and the objects you want decorated. Because then you can add as many decorators as you like and be assured that each decorator (or object to be decorated) will have all the required functionality.

 Ref: http://stackoverflow.com/questions/948443/how-to-implement-a-decorator-in-php

 We use the decorator design pattern to add new optional features to our code without changing the existing classes. The new features are added by creating new classes that belong to the same type as the existing classes.

 Ref: http://phpenthusiast.com/blog/the-decorator-design-pattern-in-php-explained

References:

1. https://en.wikipedia.org/wiki/Decorator_pattern
2. http://dsheiko.com/weblog/design-patterns-by-php-and-js-es5-examples#adapter

# The facade pattern

**Definition**

1. The front of a building, esp. an imposing or decorating one.
2. A superficial appearance or illusion of something.

Usage: 

A Facade is used when an easier or simpler interface to an underlying object is desired. Alternatively, an adapter can be used when the wrapper must respect a particular interface and must support polymorphic behaviour. A decorator makes it possible to add or alter behaviour of an interface at run-time.

References:

1. http://code.tutsplus.com/tutorials/design-patterns-the-facade-pattern--cms-22238

2. http://www.sitepoint.com/manage-complexity-with-the-facade-pattern/

3. http://en.wikipedia.org/wiki/Facade_pattern

Videos:

1. https://www.youtube.com/watch?v=WN0p2_f8MMo
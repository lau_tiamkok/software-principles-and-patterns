# The observer pattern

References:

1. http://dsheiko.com/weblog/design-patterns-by-php-and-js-es5-examples#observer

2. https://carldanley.com/js-observer-pattern/

3. http://www.dofactory.com/javascript/observer-design-pattern

4. http://bumbu.ru/javascript-observer-publish-subscribe-pattern/

5. http://stackoverflow.com/questions/12308246/how-to-implement-observer-pattern-in-javascript
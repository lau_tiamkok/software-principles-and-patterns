$( document ).ready(function() {
    
    // Define the object constructor.
    var Observable = function() {
        
        this.observers = [];
        
        return this;
        
    };
    
    // Define the public api and its public methods by extending the object.
    Observable.prototype = {
        
        addObserver : function( observer ) {
            
            this.observers.push( observer );
            
        }, 
        
        removeObserver : function( observer ) {
            
            var $root = this;
            
            for ( var i in $root.observers ) {
                
                if( $root.observers[i] === observer ) {
                    
                    $root.observers.splice( i, 1 );
                    
                }
            }
            
            //console.log( this.observers );
        }, 
        
        notifyObservers : function( arg ) {
            
            var $root = this;
            
            for ( var i in $root.observers ) {
                
                $root.observers[i].onChange( arg );
                
            }
            
        }
        
    };
    
    
    /**
    * Concrete Observable
    */
    var Foo = function() {
       
       // Call super constructor.
        Observable.call( this ); 
        
        return this;
        
    };
    
    // Subclass extends superclass.
    Foo.prototype = Object.create( Observable.prototype );
    Foo.prototype.constructor = Foo;
    
    // Subclass overwrites superclass's setModal method.
    Foo.prototype.update = function(){
        
        // Dummy model.
        var model = {
            aapl : 167.00,
            goog : 243.67,
            msft : 99.34
        };
        
        // Notify our observers.
        this.notifyObservers( model );
        
    };
    
    
    // Define a couple of different observers
    var Boo = {
        
        onChange : function(arguments) {
            
            console.log( 'Boo: ', arguments );
          
        }
        
    };
    
    var Too = {
        
        onChange : function(arguments) {
            
            console.log( 'Too: ', arguments );
          
        }
    };
    
    // Example usage
    var F = new Foo();
    F.addObserver( Boo );
    F.addObserver( Too );
    F.update();
    
    F.removeObserver( Boo );
    F.removeObserver( Too );
    F.update(); // Does nothing; no observers
    
});
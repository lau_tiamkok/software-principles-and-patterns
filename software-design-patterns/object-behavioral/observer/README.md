# The observer pattern

The observer pattern gives you another way to avoid tight coupling between components. This pattern is simple: One object makes itself observable by adding a method that allows another object, the observer, to register itself. When the observable object changes, it sends a message to the registered observers. What those observers do with that information isn't relevant or important to the observable object. The result is a way for objects to talk with each other without necessarily understanding why.

The pattern allows:

* one or **more** objects be notified of state changes in other objects within the system;

* broadcasting

References:

1. http://www.ibm.com/developerworks/library/os-php-designptrns/

2. http://dsheiko.com/weblog/design-patterns-by-php-and-js-es5-examples#observer

3. http://en.wikipedia.org/wiki/Observer_pattern

4. http://sourcemaking.com/design_patterns/observer/php
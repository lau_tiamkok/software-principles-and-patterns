<?php
interface Observer
{
    function onChanged( Observable $Observable, $args );
}

interface Observable
{
    function addObserver( Observer $Observer );
    function removeObserver( Observer $Observer );
    function notifyObserver( $args );
}

class ConcreteObservable implements Observable
{
    private $Observers = [];

    public function addItem( $name )
    {
        $this->notifyObserver( $name );
    }

    public function addObserver( Observer $Observer )
    {
        $this->Observers[] = $Observer;
    }
    
    public function notifyObserver( $options )
    {
        foreach( $this->Observers as $Observer )
        {
            $Observer->onChanged( $this, $options );   
        } 
    }
    
    function removeObserver( Observer $Observer ) 
    {
        foreach( $this->Observers as $observerKey => $observerValue ) 
        {
            if ( $observerValue === $Observer ) 
            { 
                unset( $this->Observers[ $observerKey ] );
            }
        }
    }
}

class ConcreteObserver implements Observer
{
    public function onChanged( Observable $Observable, $args )
    {
        echo "Object: ", get_class($Observable), " <br/>";
        echo( "'$args' added to user list <br/>" );
    }
}

$ConcreteObservable = new ConcreteObservable();
$ConcreteObserver = new ConcreteObserver();
$ConcreteObservable->addObserver( $ConcreteObserver );

$ConcreteObservable->addItem( "Jack" );
$ConcreteObservable->addItem( "Jane" );
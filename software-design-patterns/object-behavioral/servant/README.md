# The servant pattern

**Definition 1 :**

Servant is a design pattern used to offer some functionality to a group of classes without defining that functionality in each of them. A Servant is a class whose instance (or even just class) provides methods that take care of a desired service, while objects for which (or with whom) the servant does something, are taken as parameters.

**Definition 2 :**

The servant design pattern - or better idiom is used to provide the functionality (methods) to some group of objects. This functionality is common for all these object and therefor should not be repeated in every of these classes.

**Definition 3 :**

Servant Design Pattern provides a group of functions to objects that are passes as parameters.

**Definition 4 :**

The Servant pattern is a design pattern, used in software engineering to encapsulate a service class that can be used to perform common tasks on a set of classes, rather than repeating the code in each class. The class is passed into the servant, which performs it's service on the class.

In short, servant pattern **defines common functionality for a group of classes**.

References:

1. http://en.wikipedia.org/wiki/Servant_(design_pattern)

2. http://shimonpeter.blogspot.com/2011/11/servant-design-pattern-in-java-example.html (java)

3. http://www.jonathanhui.com/behavior-pattern (java)

4. http://social.technet.microsoft.com/wiki/contents/articles/13228.servant-design-pattern.aspx

5. https://docs.google.com/presentation/d/1V9l_L5gwG3VDaPjPpnYMKuGf48DHDSlKHsF4tdhJW_s/edit#slide=id.p
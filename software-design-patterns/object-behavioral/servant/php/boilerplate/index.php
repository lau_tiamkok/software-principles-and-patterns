<?php
// Servant.
interface Servant
{
    //  
}

// Servable. AKA Boss/ Master/ Authority
interface Servable
{
    //
}

// Concrete Servable.
class ConcreteServable implements Servable
{
    private $position;

    public function setPosition($position)
    {
        $this->position = $position . '<br/>';
    }
    
    public function getPosition()
    {
        return $this->position;
    }
}

// Concrete Servant.
class ConcreteServant implements Servant
{
    // Method, which will move Servable implementing class to position where.
    public function moveTo(Servable $Servable, $arg) 
    {
        // Do some other stuff to ensure it moves smoothly and nicely, this is
        // the place to offer the functionality.
        $Servable->setPosition($arg);
    }
}

$ConcreteServable = new ConcreteServable();
$ConcreteServant = new ConcreteServant();

$ConcreteServant->moveTo($ConcreteServable, 10);
echo $ConcreteServable->getPosition();

$ConcreteServant->moveTo($ConcreteServable, 20);
echo $ConcreteServable->getPosition();
<!DOCTYPE html>
<html>

<style>
    .element {
        border:1px solid black;
        clear:both;
    }
</style>   
<script src="jquery-min.js"></script> 
<script>
$( document ).ready(function() {
    
    console.log("approach 1:");
    function ClassName (){
        this.name   = "no name";
    }
    
    ClassName.prototype = {
        show: function() {
        },
        hide: function() {
        }
    };
    
    var InstanceOfClass = new ClassName();
    
    console.log(ClassName); // function()
    console.log(InstanceOfClass); //  Object { show=function(), hide=function()}
    
    
    console.log("approach 2:");
    var ClassName = function(){
        this.name   = "no name";
    };
    
    ClassName.prototype = {
        show: function() {
        },
        hide: function() {
        }
    };
    
    var InstanceOfClass = new ClassName();
    
    console.log(ClassName); // function()
    console.log(InstanceOfClass); //  Object { show=function(), hide=function()}
});


/*
 * @category Design Pattern Tutorial
 * @package Command Sample
 * @author Dmitry Sheiko <me@dsheiko.com>
 * @link http://dsheiko.com
 */
(function() {
/**
 * The invoker
 */
var SwitcherConstructor = function(showCommand, hideCommand){
    return {
        show: function() {
            showCommand.execute();
        },
        hide: function() {
            hideCommand.execute();
        }
    };
};
/**
 * The receiver
 */
var Widget = (function(){
    return {
        show : function() {
            console.log('Widget displayed');
        },
        hide : function() {
            console.log('Widget hidden');
        }
    };
}());
/**
 * The command to show given widget
 */
var showCommandConstructor = function(receiver) {
    return {
        execute: function() {
            receiver.show();
        }
    };
};
/**
 * The command to hide given widget
 */
var hideCommandConstructor = function(receiver) {
    return {
        execute: function() {
            receiver.hide();
        }
    };
};
 
/**
 * Usage
 */
var showCommand = new showCommandConstructor(Widget),
    hideCommand = new hideCommandConstructor(Widget),
    switcher = new SwitcherConstructor(showCommand, hideCommand);
 
switcher.show();
switcher.hide();
 
// Output:
// Widget displayed
// Widget hidden
 
}());
    
</script>

</head>
<body>
    
    <div class="element">
        1
    </div>

    <div class="element">
        2
    </div>

    <div class="element">
        3
    </div>
    
</body>
</html>
# The Command pattern

In object-oriented programming, the command pattern is a behavioral design pattern in which an object is used to represent and ** encapsulate ** all the information needed to call a method at a later time. This information includes the method name, the object that owns the method and values for the method parameters.

Four terms always associated with the command pattern are command, receiver, invoker and client. A command object has a receiver object and invokes a method of the receiver in a way that is specific to that receiver's class. The receiver then does the work. A command object is separately passed to an invoker object, which invokes the command, and optionally does bookkeeping about the command execution. Any command object can be passed to the same invoker object. Both an invoker object and several command objects are held by a client object. The client contains the decision making about which commands to execute at which points. To execute a command, it passes the command object to the invoker object. See example code below.

Using command objects makes it easier to construct general components that need to delegate, sequence or execute method calls at a time of their choosing without the need to know the class of the method or the method parameters. Using an invoker object allows bookkeeping about command executions to be conveniently performed, as well as implementing different modes for commands, which are managed by the invoker object, without the need for the client to be aware of the existence of bookkeeping or modes.

In this pattern, the Invoker knows that a Command is passed to it, without dependencies on the actual ConcreteCommand implementation. 

**Participants:**

* **Command**: defines an abstraction over a method call.

* **Concrete Command**: implementation of an operation.

* **Invoker**: refers to Command instances as its available operations.

The pattern allows:

* to decouple the invoker object from the handler object;

* multi-level undo;

* callback functionality;

* to keep history of requests;

* to handle requests in any time in any order.

References:

1. http://en.wikipedia.org/wiki/Command_pattern

2. http://dsheiko.com/weblog/design-patterns-by-php-and-js-es5-examples#observer

3. http://sourcemaking.com/design_patterns/command

4. http://www.giorgiosironi.com/2010/02/practical-php-patterns-command.html

5. http://www.newthinktank.com/2012/09/command-design-pattern-tutorial/ (java)

Videos:

1. https://www.youtube.com/watch?v=iOrhJpFW8iU&list=PL65B52FEFA0B28B2D (java)
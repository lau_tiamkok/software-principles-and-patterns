<?php
// Command.
interface Command 
{
    public function execute(Commandable $Commandable);
    public function undo(Commandable $Commandable);
}

// AKA Receiver.
interface Commandable 
{
    public function up();
    public function down();
}

// AKA Invoker
interface Commander
{
    public function press(Command $Command, Commandable $Commandable);
    public function pressUndo(Command $Command, Commandable $Commandable);
}

// Concrete command.
class ConcreteCommand implements Command 
{
    public function execute(Commandable $Commandable) 
    {
        $Commandable->up();
    }
    
    public function undo(Commandable $Commandable) 
    {
        $Commandable->down();
    }
}

// Concrete commandable.
class ConcreteCommandable implements Commandable 
{
    private $volume = 0;
    
    public function up() 
    {
        $this->volume++;
        echo "TV Volume is at: " . $this->volume . "<br/>";
    }
    
    public function down() 
    {
        $this->volume--;
        echo "TV Volume is at: " . $this->volume . "<br/>";
    }
}

// Concrete commander.
class ConcreteCommander implements Commander 
{
    public function press(Command $Command, Commandable $Commandable)
    {
        echo "Object: ", get_class($Command), " <br/>";
        echo "Object: ", get_class($Commandable), " <br/>";
        $Command->execute($Commandable); 
    }
    
    public function pressUndo(Command $Command, Commandable $Commandable)
    {
        $Command->undo($Commandable);
    }
}

// Client.
$ConcreteCommand = new ConcreteCommand();
$ConcreteCommandable = new ConcreteCommandable();
$ConcreteCommander = new ConcreteCommander();

print("Execute: <br/>");

$ConcreteCommander->press($ConcreteCommand, $ConcreteCommandable);
$ConcreteCommander->press($ConcreteCommand, $ConcreteCommandable);
$ConcreteCommander->press($ConcreteCommand, $ConcreteCommandable);
$ConcreteCommander->press($ConcreteCommand, $ConcreteCommandable);

print("Undo: <br/>");

$ConcreteCommander->pressUndo($ConcreteCommand, $ConcreteCommandable);
$ConcreteCommander->pressUndo($ConcreteCommand, $ConcreteCommandable);
$ConcreteCommander->pressUndo($ConcreteCommand, $ConcreteCommandable);
$ConcreteCommander->pressUndo($ConcreteCommand, $ConcreteCommandable);

?>
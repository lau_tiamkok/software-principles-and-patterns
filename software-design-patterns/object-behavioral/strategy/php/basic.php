<?php
interface Strategy 
{
    
}

class Foo implements Strategy 
{
    public $message = 'foo';
}

class Boo extends Foo implements Strategy 
{
    public $message = 'boo';
}

class Service
{
    public function __construct(Strategy $Strategy)
    {
        $this->Strategy = $Strategy;
    }
    
    public function fetch()
    {
        return $this->Strategy->message;
    }
}

$Service = new Service(new Boo);
var_dump($Service->fetch());


class Mammal
{
    public function walk()
    {
        
    }
}

class Human extends Mammal
{
    public function walk()
    {
        return 'Walk using two legs or whatever';
    }
}

class Cat extends Mammal
{
    public function walk()
    {
        return 'Walk using four legs or whatever';
    }
}

$mammal = new Cat;

function performWalk(Mammal $mammal){
   return $mammal->walk();
}

$walk = performWalk($mammal);

var_dump($walk);
?>
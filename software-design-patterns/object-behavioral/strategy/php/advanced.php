<?php
interface ModelStrategy 
{
   public function respond(); 
}

interface MammalStrategy 
{
    public function walk(ModelStrategy $ModelStrategy);
}

class CatModel implements ModelStrategy 
{
    public $text = 'Walk using four legs or whatever';
    
    public function respond() {
        return $this->text;
    }
}

class HumanModel implements ModelStrategy 
{
    public $text = 'Walk using 2 legs or whatever';
    
    public function respond() {
        return $this->text;
    }
}

class Cat implements MammalStrategy 
{
    public function walk(ModelStrategy $ModelStrategy)
    {
        return $ModelStrategy->respond();
    }
}

class Human implements MammalStrategy 
{
    public function walk(ModelStrategy $ModelStrategy)
    {
        return $ModelStrategy->respond();
    }
}

$mammal = new Human;

function performWalk(MammalStrategy  $mammalStrategy){
    $model = new HumanModel();
    return $mammalStrategy->walk($model);
}

$walk = performWalk($mammal);

var_dump($walk);
?>
# The visitor pattern

The Visitor pattern is a mechanism to allow external objects to access an object structure without resulting in a mutual dependency. Typically this pattern is used when dealing with a stable data structure, but a set of rapidly changing operations. The pattern sets up a class structure where it is easier to change the operations by adding new Visitors.

Every Visitor implementation has methods for visiting every concrete element type, and provide an operation. Thus what could have been a method on a class (the operation) becomes a first-class object.

The pattern allows:

* for one or **more** operations to be applied to a set of objects at run-time, decoupling the operations from the object structure.

References:

1. http://css.dzone.com/books/practical-php-patterns/practical-php-patterns-visitor

2. http://dsheiko.com/weblog/design-patterns-by-php-and-js-es5-examples#visitor

3. http://en.wikipedia.org/wiki/Visitor_pattern

Videos:

1. https://www.youtube.com/watch?v=pL4mOUDi54o (java)

Examples:

1. https://github.com/nicksp/PHP-design-patterns-1/blob/master/Visitor.php
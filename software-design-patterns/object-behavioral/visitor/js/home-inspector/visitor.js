$( document ).ready(function() {
    
    var houseWithProblems = {
        plumbing: "leaky faucet",
        garageDoor: "broken motor",
        backyard: "weed infestation",

        //accept function which will receive the visitor
        accept: function(visitor) {
            visitor.visit(this);
        }
    }



    var visitorHomeInspector = {
        visit: function(home) {

            $('#specs').append("<li class='header'>Current house status:</li>");

            //let's display this on our list
            $('#specs').append("<li> Plumbing: " + home.plumbing + "</li>");

            $('#specs').append("<li> Garage Door: " + home.garageDoor + "</li>");

            $('#specs').append("<li> Backyard: " + home.backyard + "</li>");
        }
    }

    var visitorPlumber = {
        visit: function(home) {
            //let's fix the plumbing
            home.plumbing = "All good!"
        }
    }

    var visitorElectrician = {
        visit: function(home) {
            //let's fix the garage door
            home.garageDoor = "All good!"
        }
    }

    var visitorGardener = {
        visit: function(home) {
            //let's fix the garage door
            home.backyard = "All good! No more weeds"
        }
    }



    //our event handlres for the buttons----------
    $('.display').click(function() {
        houseWithProblems.accept(visitorHomeInspector);
        $('.step1').fadeOut('slow', function() {
            $('.step2').fadeIn('slow');
        });


        //scroll to bottom
        scrollLatestStatus();
    });

    $('#fixPlumbing').click(function() {
        //just so we can see the upgraded spec
        $('#specs').append("<li class='header'>Fixing Plumbing...</li>");

        //visit from plumber
        houseWithProblems.accept(visitorPlumber);
        houseWithProblems.accept(visitorHomeInspector);
        $('.step2').fadeOut('slow', function() {
            $('.step3').fadeIn('slow');
        });


        //scroll to bottom
        scrollLatestStatus();
    });

    $('#fixGarageDoor').click(function() {
        //just so we can see the upgraded spec
        $('#specs').append("<li class='header'>Fixing Garage Door...</li>");

        //now the electrician visits
        houseWithProblems.accept(visitorElectrician);
        houseWithProblems.accept(visitorHomeInspector);

        $('.step3').fadeOut('slow', function() {
            $('.step4').fadeIn('slow');
        });


        //scroll to bottom
        scrollLatestStatus();
    });

    $('#fixGarden').click(function() {
        //just so we can see the upgraded spec
        $('#specs').append("<li class='header'>Killing garden weeds...</li>");

        //then the gardener 
        houseWithProblems.accept(visitorGardener);
        houseWithProblems.accept(visitorHomeInspector);
        $('.step4').fadeOut('slow', function() {
            $('.step5').fadeIn('slow');
        });


        //scroll to bottom
        scrollLatestStatus();
    });

    $('#startAgain').click(function() {
        startAllOverAgain();
    });

    function scrollLatestStatus() {
        $(".houseStatusContainer").animate({ scrollTop: $('#specs').height()}, 500);
    }

    function startAllOverAgain() {
        $(".steps").hide();
        $(".step1").show();
        $('#specs').append("<li class='header'>Starting All over again...</li>");
        houseWithProblems.plumbing = "leaky faucet";
        houseWithProblems.garageDoor = "broken motor";
        houseWithProblems.backyard = "weed infestation";
        scrollLatestStatus();
    }
    
});
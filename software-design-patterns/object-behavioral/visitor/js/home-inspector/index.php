<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" href="style.css">
<script src="jquery-min.js"></script>
<script src="visitor.js"></script>

</head>
<body>
    
    <div class="houseStatusContainer">
        <ul id="specs">
            <li class='header'>The House</li>
        </ul>
    </div>
    <p>
        Some back story. A house is currently visited by a home inspector. His duty is to report on some properties of the house. It appears that the house has some problems, which is why visits from different kinds of tradesmen are required.
    </p>
    <div class="step1 steps">
        <p> Start by getting the inspector to list all the house properties and its status</p>
        <input type="button" class="display" value="Check house properties" />
    </div>

    <div class="step2 steps">
        <p> Now the plumber must visit the house and fix some plumbing</p>
        <input type="button" id="fixPlumbing" value="Visit from plumber" />
    </div>

    <div class="step3 steps">
        <p> Now an electrician must fix the garage door</p>
        <input type="button" id="fixGarageDoor" value="Visit from electrician" />
    </div>

    <div class="step4 steps">
        <p> Lastly the weed problem must be solved </p>
        <input type="button" id="fixGarden" value="Visit from gardener" />  
    </div>

    <div class="step5 steps">
        <input type="button" id="startAgain" value="Start all over again!" />
    </div>
</body>
</html>
## The House

Let�s start with building our house class with a couple of problems to start with. Well, maybe we bought an old house then�.since it has a couple issues to begin with. Anyway, the house object will look like this.

```
var houseWithProblems = {
    plumbing: "leaky faucet",
    garageDoor: "broken motor",
    backyard: "weed infestation",
 
    //accept function which will receive the visitor
    accept: function(visitor) {
        visitor.visit(this);
    }
}
```

## The Visitors: Home Inspector

The home inspector (could be ourselves or someone else) is a person who checks the house for the curent problems. In our example, he simply writes in our list the current status of the house elements.

```
var visitorHomeInspector = {
    visit: function(home) {

        $('#specs').append("<li class='header'>Current house status:</li>");

        //let's display this on our list
        $('#specs').append("<li> Plumbing: " + home.plumbing + "</li>");

        $('#specs').append("<li> Garage Door: " + home.garageDoor + "</li>");

        $('#specs').append("<li> Backyard: " + home.backyard + "</li>");
    }
}
```

## The Visitors: Tradesmen

```
var visitorPlumber = {
    visit: function(home) {
        //let's fix the plumbing
        home.plumbing = "All good!"
    }
}
 
var visitorElectrician = {
    visit: function(home) {
        //let's fix the garage door
        home.garageDoor = "All good!"
    }
}
 
var visitorGardener = {
    visit: function(home) {
        //let's fix the garage door
        home.backyard = "All good! No more weeds"
    }
}
```

## The Action Call

The action calls consist of simple jQuery click even handler in our example. We�ll take the display action for example.

```
$('.display').click(function() {
    houseWithProblems.accept(visitorHomeInspector);
    $('.step1').fadeOut('slow', function() {
        $('.step2').fadeIn('slow');
    });

    //scroll to bottom
    scrollLatestStatus();
});
```

So to recap what we have just discussed about visitor pattern, here are a few key points:

* Visitor pattern gives us the ability to add much more functionality to an object, without having to modify the object�s class codes
* To use this technique we need the main object to visit and visitor objects
* The main object to visit must have the method accept to be able to receive visitors
* The visitor objects must have the method visit to be called by the accept method of the main object
* In conclusion, visitor pattern is extremely handy. However one must be careful when using this technique as overzealous use of visitors might lead to spaghetti code and confusion in the future.

References:

1. http://www.codingepiphany.com/2013/04/13/back-to-basics-visitor-pattern-explanation-with-js-example/
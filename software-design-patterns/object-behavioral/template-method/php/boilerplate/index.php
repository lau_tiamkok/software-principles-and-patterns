<?php
// Abstract template method.
abstract class AbstractTemplate
{
    protected $images = array();
    
    final public function render() 
    {
        
        $output = '<div id="slider">';
        foreach ($this->images as $image) {
            $output .= '<img src="' . $image . '">';
        }
        $output .= "</div>";
        return $output;
        
    }
}

// Concrete template.
class ConcreteTemplate1 extends AbstractTemplate
{
    protected $images = array(
        "sample_image1.jpg",
        "sample_image2.jpg",
        "sample_image3.jpg",
        "sample_image4.jpg",
        "sample_image5.jpg"
    );
}

// Concrete template.
class ConcreteTemplate2 extends AbstractTemplate
{
    protected $images = array(
        "sample_image6.jpg",
        "sample_image7.jpg",
        "sample_image8.jpg"
    );
}

$ConcreteTemplate1 = new ConcreteTemplate1();
echo $ConcreteTemplate1->render();

$ConcreteTemplate2 = new ConcreteTemplate2();
echo $ConcreteTemplate2->render();
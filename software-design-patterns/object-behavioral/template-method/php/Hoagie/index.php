<?php
// Abstract method.
abstract class Hoagie
{
    abstract function addMeat();
    abstract function addCheese();
    abstract function addVeggies();
    
    final public function makeSandwich() 
    {
        
        $this->cutBun();
        
        echo "<br/>";
        
        if ($this->customerWantsMeat()) 
        {
            $this->addMeat();
            echo "<br/>";
        }
        
        if ($this->customerWantsCheese()) 
        {
            $this->addCheese();
            echo "<br/>";
        }
        
        if ($this->customerWantsVeggies()) 
        {
            $this->addVeggies();
            echo "<br/>";
        }
        
        $this->wrapHoagie();
    }
    
    private function cutBun() 
    {
        echo "Bun is cut.";
    }
    
    private function wrapHoagie() 
    {
        echo "Hoagie is wrapped.";
    }

    public function customerWantsMeat() 
    {
        return true;
    }
    
    public function customerWantsCheese() 
    {
        return true;
    }
    
    public function customerWantsVeggies() 
    {
        return true;
    }
}

// Concrete template method.
class ItalianHoagie extends Hoagie
{
    private $meatUsed = array("Salami","Pepper");
    private $cheeseUsed = array("Provolone");
    private $veggiesUsed = array("Lettuce","Tomatoes","Onions");#
    
    public function addMeat() 
    {
        echo "Adding meat: <br/>";
        
        foreach($this->meatUsed as $meatUsed)
        {
            echo $meatUsed . " ";
        }
    }
    
    public function addCheese()
    {
        echo "Adding cheese: <br/>";
        
        foreach($this->cheeseUsed as $cheeseUsed)
        {
            echo $cheeseUsed . " ";
        }
    }
    
    public function addVeggies()
    {
        echo "Adding veggies: <br/>";
        
        foreach($this->veggiesUsed as $veggiesUsed)
        {
            echo $veggiesUsed . " ";
        }
    }
}

// Concrete template method.
class VeggieHoagie extends Hoagie
{
    private $veggiesUsed = array("Lettuce","Tomatoes","Onions");#
    
    public function addMeat() 
    {
    }
    
    public function addCheese()
    {
    }
    
    public function addVeggies()
    {
        echo "Adding veggies: <br/>";
        
        foreach($this->veggiesUsed as $veggiesUsed)
        {
            echo $veggiesUsed . " ";
        }
    }
    
    public function customerWantsMeat() 
    {
        return false;
    }
    
    public function customerWantsCheese() 
    {
        return false;
    }
}

$ItalianHoagie = new ItalianHoagie();
$ItalianHoagie->makeSandwich();
echo "<br/><br/>";
$VeggieHoagie = new VeggieHoagie();
$VeggieHoagie->makeSandwich();
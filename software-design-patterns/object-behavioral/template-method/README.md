# The Template Method

In the Template Pattern an abstract class will define a method with an algorithm, and methods which the algorithm will use. The methods the algorithm uses can be either required or optional. The optional method should by default do nothing.

References:

1. http://en.wikipedia.org/wiki/Template_method_pattern

2. http://sourcemaking.com/design_patterns/template_method/php

3. http://www.giorgiosironi.com/2010/04/practical-php-patterns-template-method.html

4. http://www.sitepoint.com/overriding-strategy-logic-the-template-method-pattern/

5. https://www.youtube.com/watch?v=aR1B8MlwbRI (Java)

6. http://stackoverflow.com/questions/672083/when-to-use-template-method-vs-strategy
# Single responsibility principle (SRP)

a class should have only a single responsibility (i.e. only one potential change in the software's specification should be able to affect the specification of the class).

# References

1. http://code.tutsplus.com/tutorials/solid-part-1-the-single-responsibility-principle--net-36074
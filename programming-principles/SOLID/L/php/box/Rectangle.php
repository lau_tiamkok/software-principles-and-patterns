<?php
class Rectangle extends Box
{  
    /* Draw Rectangle */
    public function __construct()
    {
        $this->doBox();
    }
    
    //Set data for Rectangle
    protected function setData()
    {
        $this->xpos=20;
        $this->ypos=20;
        $this->wide=200;
        $this->high=150;
        $this->fill="#CFD7D9";
        $this->stroke="#000";
        $this->strWidth=1;
    }
}
?>

<?php
error_reporting(E_ALL | E_STRICT);
ini_set("display_errors", 1);
function __autoload($class_name) 
{
    include $class_name . '.php';
}
class Client
{
    private $subLiskov;
    
    public function __construct()
    {
        $this->doRectangle();
        $this->doSquare();
    }
    
    private function doSquare()
    {
        $this->subLiskov = new Square();
        $this->boxWork($this->subLiskov); 
    }
    
    private function doRectangle()
    {
        $this->subLiskov = new Rectangle();
        $this->boxWork($this->subLiskov); 
    }
    
    //The type hint 'Box' insures the same parent class
    private function boxWork(Box $box)
    {
        echo $box->doBox();
    }
}
$worker = new Client();
?>


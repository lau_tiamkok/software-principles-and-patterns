<?php
class Square extends Box
{
    /* Draw Square */
    public function __construct()
    {
        $this->doBox();
    }
    
    //Set values for square
    protected function setData()
    {
        $this->xpos=20;
        $this->ypos=20;
        $this->wide=150;
        $this->high=150;
        $this->fill="#6FA3CC";
        $this->stroke="#000";
        $this->strWidth=1;
    }
}
?>

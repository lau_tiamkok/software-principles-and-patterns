<?php
abstract class Box
{
    //Abstract method 
    abstract protected function setData();
    //Concrete method
    public function doBox()
    {  
        $this->setData();
        $this->box=<<<BOX
         <!doctype html><html><head>
        <meta charset='UTF-8'></head><body>
        <svg width='30%' height='25%' xmlns='http://www.w3.org/2000/svg' version='1.1'>
        <rect x='$this->xpos' y='$this->ypos' width='$this->wide' height='$this->high' fill='$this->fill' stroke='$this->stroke' stroke-width='$this->strWidth' />
        </svg>
        </body></html>
BOX;
    return $this->box;
    }
    
    //Properties
    protected $xpos;
    protected $ypos;
    protected $wide;
    protected $high;
    protected $fill;
    protected $stroke;
    protected $strWidth;
    protected $box;    
}
?>
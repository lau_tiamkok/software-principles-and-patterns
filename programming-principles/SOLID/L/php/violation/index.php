<?php
// Violation of Likov's Substitution Principle
class Rectangle
{
	protected $width;
	protected $height;

	public function setWidth($width){
		$this->width = $width;
	}

	public function setHeight($height){
		$this->height = $height;
	}


	public function getWidth(){
		return $width;
	}

	public function getHeight(){
		return $height;
	}

	public function getArea(){
		return $this->width * $this->height;
	}	
}

class Square extends Rectangle 
{
	public function setWidth($width){
		$this->width = $width;
		$this->height = $width;
	}

	public function setHeight($height){
		$this->width = $height;
		$this->height = $height;
	}

}

$Rectangle = new Square();
$Rectangle->setWidth(5);
//$Rectangle->setHeight(10);
echo $Rectangle->getArea();
?>
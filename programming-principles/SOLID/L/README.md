# Liskov substitution principle (LSP)

"objects in a program should be replaceable with instances of their subtypes without altering the correctness of that program."

"Let q(x) be a property provable about objects x of type T. Then q(y) should be provable for objects y of type S, where S is a subtype of T."

LSP in other word - **substitutability**.

Liskov's notion of a behavioral subtype defines a notion of substitutability for mutable objects; that is, if S is a subtype of T, then objects of type T in a program may be replaced with objects of type S without altering any of the desirable properties of that program (e.g., correctness).

"Subtypes must be substitutable for their base types." - Robert C. Martin

As simple as that, a subclass should override the parent class' methods in a way that does not break functionality from a client's point of view. In other words, it states that **objects in a program should be replaceable with instances of their subtypes without altering the correctness of the program**.

# References

1. http://code.tutsplus.com/tutorials/solid-part-3-liskov-substitution-interface-segregation-principles--net-36710

2. http://en.wikipedia.org/wiki/Liskov_substitution_principle

3. http://www.php5dp.com/oop-principles-the-liskov-substitution-principle/

4. http://www.oodesign.com/liskov-s-substitution-principle.html (Java)

5. http://williamdurand.fr/2013/07/30/from-stupid-to-solid-code/#liskov-substitution-principle
# Dependency inversion principle (DIP)

one should "Depend upon Abstractions. Do not depend upon concretions."

"A. High-level modules should not depend on low-level modules. Both should depend on abstractions."

"B. Abstractions should not depend upon details. Details should depend upon abstractions."

# References

1. http://code.tutsplus.com/tutorials/solid-part-4-the-dependency-inversion-principle--net-36872

2. http://www.brandonsavage.net/the-d-doesnt-stand-for-dependency-injection/
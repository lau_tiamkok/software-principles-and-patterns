# Open/closed principle (SRP)

�software entities � should be open for extension, but closed for modification.�

# References

1. http://code.tutsplus.com/tutorials/solid-part-2-the-openclosed-principle--net-36600
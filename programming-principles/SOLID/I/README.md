# Interface segregation principle (ISP)

�many client-specific interfaces are better than one general-purpose interface.�

# References

1. http://code.tutsplus.com/tutorials/solid-part-3-liskov-substitution-interface-segregation-principles--net-36710
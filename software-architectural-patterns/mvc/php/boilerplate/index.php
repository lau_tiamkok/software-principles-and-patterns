<?php 
//Model
class Model
{
    public $string;
 
    public function __construct(){
        $this->string = "MVC + PHP = Awesome, click here!";
    }
 
}



//View 
class View
{
    private $model;
 
    public function __construct($model) {
        //$this->controller = $controller;
        $this->model = $model;
    }
 
    public function output() {
        return '<p><a href="index.php?action=clicked">' . $this->model->string . "</a></p>";
    }
}


//Controller
class Controller
{
    private $model;
 
    public function __construct($model){
        $this->model = $model;
    }
 
    public function clicked() {
        $this->model->string = "Updated Data, thanks to MVC and PHP!";
    }
}


//Application initialisation/entry point. In Java, this would be the static main method.
$model = new Model();
$controller = new Controller($model);
$view = new View($model);
 
if (isset($_GET['action']) && !empty($_GET['action'])) {
    $controller->{$_GET['action']}();
}
 
echo $view->output();

?>

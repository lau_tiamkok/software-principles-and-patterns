$( document ).ready(function() {
    
    /* ==========================================================================
    * A "Hello World"-like example of Javascript using the MVC pattern.
    * ========================================================================== */

    /*
    * Model
    */

   // a model is where the data object is created.
   var ModelExample = function (  ) {
       
        // the model instance has a property called "myProperty"
        // created from the data's "yourProperty".
        this.myProperty = 'something';

        // return the model instance
        return this;
   };

   // a model constructor might have a function that populate the data.
   ModelExample.prototype.find = function ( id ) {
       
        // data used to create a new model may come from anywhere
        // but in this example data comes from this inline object.
        var ourData = {
            '123': {
                    yourProperty: 'Hello World'
            },
            '124': {
                    yourProperty: 'Hello World!!!'
            }
        };

        this.myProperty = ourData[id];

        // return the model.
        return this;
   };


   /*
    * View
    */

   // a view is where the output is created.
   var ViewExample = function ( model ) {
       
        this.model = model;

        return this;
   };

   // a view might have a function that returns the rendered output.
   ViewExample.prototype.output = function () {
       
        // data used to create a template may come from anywhere
        // but in this example template comes from this inline string.
        var ourData = '<h1>'+this.model.myProperty.yourProperty+'</h1>';

        // return the template using values from the model.
        return ourData;
   };

   // a view might have a function that renders the output.
   ViewExample.prototype.render = function () {
       
        // this view renders to the element with the id of "output"
        document.getElementById('output').innerHTML = this.output();
   };



   /*
    * Controller
    */

   // a controller is where the model and the view are used together.
   var ControllerExample = function (model) {
       this.model = model;
       return this;
   };

   // this function uses the Model and View together.
   ControllerExample.prototype.getItem = function ( id ) {
       
        // get the model.
        return this.model.find( id );
   };




   /*
    * Example
    */

   function bootstrapper() {
       
        // get the model.
        var model = new ModelExample();
           
        var controller = new ControllerExample(model);
        controller.getItem(124);
        
        // get a new view.
        var view = new ViewExample(model);
        view.render();
   }

   bootstrapper();
    
});
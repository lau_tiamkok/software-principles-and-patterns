<!DOCTYPE html>
<html>
<link rel="stylesheet" type="text/css" href="style.css">
<script src="jquery-min.js"></script>
<script src="app.js"></script>

</head>
<body>
    
    <div class="row container">
        <div class="col-xs-4">
            <select id="list" class="form-control" size="10"></select>
        </div>
        <div class="col-xs-8">
            <button id="plusBtn" class="btn btn-default btn-block">+</button>
            <button id="minusBtn" class="btn btn-default btn-block">-</button>
        </div>
    </div>
</body>
</html>
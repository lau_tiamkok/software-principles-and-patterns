# Modelviewcontroller

Modelviewcontroller (MVC) is a software architectural pattern for implementing user interfaces. It divides a given software application into three interconnected parts, so as to separate internal representations of information from the ways that information is presented to or accepted from the user.

## The controller:view relationship is 1:1

If you've studied at a lot of the popular MVC frameworks this will look familiar. What's wrong with it? Firstly, the controller now has two responsibilities: editing and listing users, causing unnecessary repeated code. The other result of this is that because the view could be anything it forces binding logic into the controller which is bad

On the web there is no reason at all that the controller even needs to know about the view. In desktop applications, the controller must be able to inform the view to refresh after a user-action. On the web, the view is always refreshed anyway so this entire relationship is irrelevant.

By completely decoupling the view from the controller, there are several advantages:

* The view selection logic exists outside the controller adding increased flexibility. The controller doesn't care at all about what the view is doing, or even if it exists.
* The binding logic in the controller can be removed
* Each controller now has zero knowledge for the view. This is good because the view can then be substituted without making any changes to the controller
* Smaller, simpler controllers with less repeated code. The controller should never call render or select a view.

ref: https://r.je/mvc-php-front-controller.html

## References:

1. http://aspiringcraftsman.com/2007/08/25/interactive-application-architecture/

2. https://r.je/views-are-not-templates.html

3. https://r.je/mvc-tutorial-real-application-example.html

4. http://www.sitepoint.com/the-mvc-pattern-and-php-1/

5. http://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller

6. http://david-franklin.net/programming/misunderstood-php-mvc/

7. http://stackoverflow.com/questions/6854960/are-most-php-frameworks-actually-mva-instead-of-mvc

8. http://haacked.com/archive/2008/06/16/everything-you-wanted-to-know-about-mvc-and-mvp-but.aspx/

9. https://lukehalliwell.wordpress.com/2008/08/30/model-view-controller-is-extinct/

10. http://blog.turn.tw/?p=1539

11. http://www.ithome.com.tw/node/77330

12. http://www.bennadel.com/blog/2379-a-better-understanding-of-mvc-model-view-controller-thanks-to-steven-neiland.htm

### Folder Structure

1. http://stackoverflow.com/questions/5863870/how-should-a-model-be-structured-in-mvc

2. http://stackoverflow.com/questions/7959673/directory-structure-for-mvc

3. http://framework.zend.com/manual/1.12/en/project-structure.project.html

4.  https://www.youtube.com/watch?v=2mBPHBPT9kY (video)

5. https://www.youtube.com/watch?v=hTxSxTpSi2w (video)
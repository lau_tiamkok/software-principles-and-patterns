<?php
use Barium\TraityMethod\ArrayMethods;
use Barium\TraityMethod\ObjectMethods;
use Barium\TraityMethod\itemMethods;

class ArticleMapper
{
    use ItemMethods;
    use ArrayMethods;
    use ObjectMethods;
    
    public $Database;
    
    public function __construct(Database $pdo)
    {
        $this->Database = $pdo;
    }
    
    public function getRow($article_id)
    {
        $sql = "
            SELECT *
            FROM article AS p
            WHERE article_id = ?
        ";
        
        $this->item = $this->Database->fetchRowObject($sql, $article_id);
        
        return $this;
    }
    
    public function addContent()
    {
        $ArticleContent = new ArticleContent($this->Database);
                
        // Set an array.
        $content = [];

        // Set a variable for storing the original data.
        $item = $this->item;

        // Fetch the content rows.
        $items_content = $ArticleContent->getRows(array(
            "article_id" 	=>	$this->item->article_id
        ))->getItems();

        // Re-structure the content key(code) and value.
        foreach($items_content as $index => $item_content)
        {
            // Always make the first item as 'content'.
            if($index === '0') $content['content'] = $item_content->value;

            $content[$item_content->code] = $item_content->value;
        }
        //var_dump($content);

        // Reset $this->item to the original.
        $this->item = $this->arrayToObject(array_merge($this->objectToArray($item),$this->objectToArray($content)));

        // Unset.
        unset($content);

        // Return this for method chaining.
        return $this;
    }
    
    public function mapRow(ArticleModel $ArticleModel, $article_id)
    {
       $ArticleModel->item = $this->getRow($article_id)->getItem();
    }
}

class ArticleContent
{
    use ItemMethods;
    
    // Set local property.
    protected $Database;
    
    /**
     * extend the parent class property.
     */	
    public function __construct($Database)
    {
        // Set the dependency injection.
        $this->Database = $Database;
    }
    
    /**
     * The "get" action.
     * @param string,array $data
     * @param array $options
     * @return object $method's object{$items, $item, $total}
     */
    public function getRows($options = array())
    {
        $sql= "
            SELECT*
            FROM 
            (
                SELECT *
                FROM category AS a
                WHERE a.type = 'content'
            ) a
            LEFT JOIN
            (
                SELECT c.*
                FROM content AS c

                LEFT JOIN article_has_content AS x
                ON x.content_id = c.content_id

                WHERE x.article_id = ?
            ) b
            ON b.category_id = a.category_id
        ";

        $this->total = $this->Database->countRows($sql,array(
            $options['article_id']
        ));
        
        $this->items = $this->Database->fetchRows($sql,array(
            $options['article_id']
        ));

        return $this;
    }
}
?>
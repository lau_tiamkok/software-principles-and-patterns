<?php
class LocalArticleMapper extends ArticleMapper
{
    public function __construct(Database $Database, $LoggerModel)
    {
        $this->Database = $Database;
        $this->Logger = $LoggerModel;
    }
    
    public function mapRow(ArticleModel $ArticleModel, $article_id)
    {
       $ArticleModel->item = $this->getRow($article_id)->addContent()->getItem();
    }
}
?>
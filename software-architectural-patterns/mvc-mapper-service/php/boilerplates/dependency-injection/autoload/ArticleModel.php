<?php
use Barium\TraityMethod\ArrayMethods;
use Barium\TraityMethod\ObjectMethods;

class ArticleModel
{
    use ArrayMethods;
    use ObjectMethods;
    
    public function respond()
    {
        $this->dataToProperty($this->item);
        unset($this->item);
    }
    
    public function __invoke()
    {
        $this->respond();
    }   
}
?>
<?php 
function __autoload($classname) 
{
    $namespace = "\\";
    $parts = explode($namespace, $classname);
    
    // Set the class file name.
    $filename = "autoload/".end($parts).'.php';
    //var_dump($filename);
    
    if (is_readable($filename)) 
    {
        require_once $filename;
    }
}

// Host used to access Database.
define('DB_HOST', 'localhost');

// Username used to access Database.
define('DB_USER', 'root');

// Password for the username.
define('DB_PASS', 'tklau');

// Name of your databse.
define('DB_NAME', 'cms_master'); 

// Data source name.
define('DSN', 'mysql:host='.DB_HOST.';dbname='.DB_NAME);

// Instance of Database.
$Database = new Database(DSN,DB_USER,DB_PASS);

// Make connection.
$Database->connect();

$LoggerModel = [];

$ArticleModel = new ArticleModel();
$ArticleMapper = new LocalArticleMapper($Database, $LoggerModel);
$ArticleService = new ArticleService($ArticleMapper, $ArticleModel);
//$article = $ArticleService->fetchRow(4);

$ArticleController = new ArticleController($ArticleService);
$ArticleController->request(4);

$ArticleView = new ArticleView($ArticleModel);
$ArticleView->output();

var_dump(get_class_methods($Database));
var_dump(get_class_methods($ArticleModel));
var_dump(get_object_vars($ArticleModel));
?>
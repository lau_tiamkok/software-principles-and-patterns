<?php
class ArticleService
{
    public function __construct($ArticleMapper, $ArticleModel)
    {
        $this->ArticleMapper = $ArticleMapper;
        $this->ArticleModel = $ArticleModel;
    }
    
    public function fetchRow($article_id)
    {
        $this->ArticleMapper->mapRow($this->ArticleModel,$article_id);
        $this->ArticleModel->respond();
        return $this->ArticleModel;
        
        /* If you are going to use __invoke method:
        $ArticleModel = $this->ArticleModel;
        $ArticleModel();
        return $ArticleModel;
         * 
         */
    }
}
?>
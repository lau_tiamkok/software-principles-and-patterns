<?php
class ArticleController
{
    public $ArticleService;
    
    public function __construct($ArticleService)
    {
        $this->ArticleService = $ArticleService;
    }
    
    public function request($id)
    {
        $this->ArticleService->fetchRow($id);
    }
}
?>
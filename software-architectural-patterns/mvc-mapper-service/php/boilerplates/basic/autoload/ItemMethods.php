<?php
/**
 * Common methods.
 */
namespace Barium\TraityMethod;

// droplet - a small drop of liquid
// snippet - a small and often interesting piece of news, information, or conversation
// driblet - a thin stream or small drop of liquid
// flake - a small, thin piece of something, especially if it has come from a surface covered with a layer of something
trait ItemMethods
{
    public function getItems($type = null)
    {
        if($type === 'array')
        {
            return $this->toBeArray($this->items);
        }
        elseif($type === 'json')
        {
            return $this->toBeJson($this->items);
        }
        elseif($type === 'object')
        {
            return $this->items;
        }
        return $this->items;
    }
    
    /*
     * Get the data of a row from $this.
     * @return object.
     */
    public function getItem($type = null)
    {
        if($type === 'array')
        {
            return $this->toBeArray($this->item);
        }
        elseif($type === 'json')
        {
            return $this->toBeJson($this->item);
        }
        elseif($type === 'object')
        {
            return $this->item;
        }
        return $this->item;
    }
    
    /*
     * Convert data to object.
     */
    function toBeObject($item) 
    {
        return $this->arrayToObject($item);
    }

    /*
     * Convert data to array.
     */
    function toBeArray($item) 
    {
        return $this->objectToArray($item);
    }

    /*
     * Convert data to json.
     */
    function toBeJson($item) 
    {
        if(is_object($item) === true) 
        {
            $item = $this->objectToArray($item);
        }

        if(is_array($item) === true) 
        {
            $item = json_encode($item);
        }

        return $item; 
    }
}
?>
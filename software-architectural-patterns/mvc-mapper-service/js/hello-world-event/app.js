$( document ).ready(function() {
    
    /* ========================================================================================
    * A "Hello World"-like example of Javascript using the MVC + Service + Mapper pattern.
    * ======================================================================================== */
   
   /*
    * Service
    */
   
   var ServiceExample = function ( mapper, model ) {
       
        this.mapper = mapper;
        this.model = model;
       
        return this;
   };
   
   ServiceExample.prototype.find = function ( id ) {
       
        this.mapper.populate(this.model, id);

        return this;
   };
   
   /*
    * Mapper
    */
   
   var MapperExample = function (  ) {
       
        return this;
   };
   
   MapperExample.prototype.populate = function ( model, id ) {
       
        // data used to create a new model may come from anywhere
        // but in this example data comes from this inline object.
        var ourData = {
            '123': {
                    message: 'Hello World'
            },
            '124': {
                    message: 'Hello World!!!'
            }
        };

        if(id) {
             model.collection = ourData[id];
        } 
        
        return this;
   };

    /*
    * Model
    */

   // a model is where the data object is created.
   var ModelExample = function (  ) {
       
        // the model instance has a property called "collection"
        // created from the data's "message".
        this.collection = {
            message: 'Nothing!'
        };

        // return the model instance
        return this;
   };

   ModelExample.prototype.getName = function ( id ) {
        
        // somethong
        // return the model.
        return this;
   };

   /*
    * View
    */

   // a view is where the output is created.
   var ViewExample = function ( model, controller ) {
       
        this.model = model;
        this.controller = controller;
        
        // binding events as soon as the object is instantiated
        this.bindEvents();

        return this;
   };

    // a view might have a function that returns the rendered output.
    ViewExample.prototype.output = function () {

         // data used to create a template may come from anywhere
         // but in this example template comes from this inline string.
         var ourData = '<h1>'+this.model.collection.message+'</h1>';

         // return the template using values from the model.
         return ourData;
    };

    // a view might have a function that renders the output.
    ViewExample.prototype.render = function () {

         // this view renders to the element with the id of "output"
         document.getElementById('output').innerHTML = this.output();
    };

    ViewExample.prototype.bindEvents = function() {
        
        var $root = this;
        
        // Place all your event bindings in one place and call them out
        // in their own methods as needed.
        $('#search-button').on('click', function(){
            console.log('onclicked');
            var id = $("#input-text").val();
            $root.controller.find( id );
            $root.render();
        });
        
        $('#input-text').on('change', function(){
            console.log('onchanged');
            var id = $(this).val();
            $root.controller.find( id );
            $root.render();
        });
   };

   /*
    * Controller
    */

   var ControllerExample = function (service) {
       this.service = service;
       return this;
   };

   ControllerExample.prototype.find = function ( id ) {
       
        return this.service.find( id );
   };

   /*
    * Example
    */

   function bootstrapper() {
       
        // clone a model.
        var model = new ModelExample();
        
        // clone a mapper.
        var mapper = new MapperExample();
        
        // clone a service.
        var service = new ServiceExample( mapper, model);
        
        // clone a controller.
        var controller = new ControllerExample(service);
        controller.find();
        
        // clone a view.
        var view = new ViewExample(model, controller);
        view.render();
        
   }

   bootstrapper();
    
});
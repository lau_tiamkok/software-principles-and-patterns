$( document ).ready(function() {
    
    /* ========================================================================================
    * A "Hello World"-like example of Javascript using the MVC + Service + Mapper pattern.
    * ======================================================================================== */
   
   /*
    * Service
    */
   var ServiceExample = function ( mapper, model ) {
       
        this.mapper = mapper;
        this.model = model;
       
        // return the model instance
        return this;
   };
   
   ServiceExample.prototype.find = function ( id ) {
       
        this.mapper.populate(this.model, id);

        return this;
   };
   
   
   /*
    * Mapper
    */
   var MapperExample = function (  ) {
       
        return this;
   };
   
   // a model constructor might have a function that populate the data.
   MapperExample.prototype.populate = function ( model, id ) {
       
        // data used to create a new model may come from anywhere
        // but in this example data comes from this inline object.
        var ourData = {
            '123': {
                    yourProperty: 'Hello World'
            },
            '124': {
                    yourProperty: 'Hello World!!!'
            }
        };

        model.myProperty = ourData[id];

        // return the model.
        return this;
   };
   

    /*
    * Model
    */

   // a model is where the data object is created.
   var ModelExample = function (  ) {
       
        // the model instance has a property called "myProperty"
        // created from the data's "yourProperty".
        this.myProperty = 'something';

        // return the model instance
        return this;
   };

   // a model constructor might have a function.
   ModelExample.prototype.getName = function ( id ) {
        
        // somethong
        // return the model.
        return this;
   };


   /*
    * View
    */

   // a view is where the output is created.
   var ViewExample = function ( model ) {
       
        this.model = model;

        return this;
   };

   // a view might have a function that returns the rendered output.
   ViewExample.prototype.output = function () {
       
        // data used to create a template may come from anywhere
        // but in this example template comes from this inline string.
        var ourData = '<h1>'+this.model.myProperty.yourProperty+'</h1>';

        // return the template using values from the model.
        return ourData;
   };

   // a view might have a function that renders the output.
   ViewExample.prototype.render = function () {
       
        // this view renders to the element with the id of "output"
        document.getElementById('output').innerHTML = this.output();
   };



   /*
    * Controller
    */

   var ControllerExample = function (service) {
       this.service = service;
       return this;
   };

   ControllerExample.prototype.find = function ( id ) {
       
        return this.service.find( id );
   };




   /*
    * Example
    */

   function bootstrapper() {
       
        // get a new model.
        var model = new ModelExample();
        
        // get a new model.
        var mapper = new MapperExample();
        
        // get a new service.
        var service = new ServiceExample( mapper, model);
           
        var controller = new ControllerExample(service);
        controller.find(123);
        
        // get a new view.
        var view = new ViewExample(model);
        view.render();
   }

   bootstrapper();
    
});